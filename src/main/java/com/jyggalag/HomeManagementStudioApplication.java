package com.jyggalag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeManagementStudioApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeManagementStudioApplication.class, args);
	}

}
