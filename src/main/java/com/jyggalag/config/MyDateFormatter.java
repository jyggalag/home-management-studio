package com.jyggalag.config;

import java.time.format.DateTimeFormatter;

public class MyDateFormatter {

    public static DateTimeFormatter withHoursFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd, HH:mm");
    }

    public static DateTimeFormatter noHoursFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }
}
