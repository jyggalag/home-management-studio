package com.jyggalag.controllers;

import com.jyggalag.config.MyDateFormatter;
import com.jyggalag.models.Announcement;
import com.jyggalag.repositories.AnnouncementRepository;

import com.jyggalag.services.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/announcements")
public class AnnouncementsController {

    private AnnouncementService announcementService;

    @Autowired
    public void setAnnouncementService(AnnouncementService announcementService) {
        this.announcementService = announcementService;
    }

    @GetMapping()
    public String listAnnouncements(Model model, Principal principal, Pageable pageable) {
        Page<Announcement> announcementPage = announcementService.findAll(pageable);
        PageWrapper<Announcement> page = new PageWrapper<>(announcementPage, "/announcements");

//        announcements.sort(Comparator.comparing(Announcement::getId).reversed());
//        page.getContent().sort(Comparator.comparing(Announcement::getId).reversed());
        model.addAttribute("announcementList", page.getContent());
        model.addAttribute("page", page);
        model.addAttribute("loggedUser", principal.getName());
        return "announcement/list";
    }

    @GetMapping("/add")
    public String moveToAdd(Model model, Principal principal) {
        model.addAttribute("announcementModel", new Announcement());
        model.addAttribute("loggedUser", principal.getName());
        return "announcement/add";
    }

    @PostMapping("/add")
    public String addAnnouncement(@ModelAttribute Announcement announcementModel, RedirectAttributes redirectAttributes, Principal principal) {
        announcementModel.setDate(LocalDateTime.now().format(MyDateFormatter.withHoursFormatter()));
        announcementModel.setOwner(principal.getName());
        announcementService.saveAnnouncement(announcementModel);
        redirectAttributes.addFlashAttribute("message", "Pomyślnie dodano ogłoszenie");
        return "redirect:/announcements";
    }

    @Transactional
    @GetMapping("/deleteAnnouncement/{id}")
    public String deleteAnnouncement(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        long announcementId = Long.parseLong(id);
        announcementService.deleteAnnouncementById(announcementId);
        redirectAttributes.addFlashAttribute("message", "Usunięto ogłoszenie");
        return "redirect:/announcements";
    }

}
