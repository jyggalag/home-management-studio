package com.jyggalag.controllers;

import com.jyggalag.config.MyDateFormatter;
import com.jyggalag.models.Gift;
import com.jyggalag.repositories.GiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/gifts")
public class GiftsController {
    private GiftRepository giftRepository;
    @Autowired
    public GiftsController(GiftRepository giftRepository) {
        this.giftRepository = giftRepository;
    }

    @GetMapping
    public String showGiftsList(Model model, Principal principal) {
        List<Gift> gifts = giftRepository.findAll();
        gifts.sort(Comparator.comparing(Gift::getOrderStatus));
        model.addAttribute("giftList", gifts);
        model.addAttribute("loggedUser", principal.getName());
        return "gifts/list";
    }

    @GetMapping("/add")
    public String addGift(Model model, Principal principal) {
        model.addAttribute("giftModel", new Gift());
        model.addAttribute("loggedUser", principal.getName());
        return "gifts/add";
    }

    @PostMapping("/add")
    public String saveGift(@ModelAttribute Gift giftModel, RedirectAttributes redirectAttributes, Principal principal) {
        giftModel.setOwner(principal.getName());
        giftModel.setDateStart(LocalDateTime.now().format(MyDateFormatter.withHoursFormatter()));
        giftModel.setOrderStatus(0);
        giftModel.setReceiveStatus(0);
        giftModel.setDateOrder("");
        giftRepository.save(giftModel);
        redirectAttributes.addFlashAttribute("message", "Pomyślnie dodano życzenie");
        return "redirect:/gifts";
    }

    @Transactional
    @GetMapping("/setOrderStatus/{id}")
    public String changeOrderStatus(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        long giftId =  Long.parseLong(id);
        giftRepository.changeOrderStatus(giftId);
        redirectAttributes.addFlashAttribute("message", "Zmieniono status");
        return "redirect:/gifts";
    }

    @Transactional
    @GetMapping("/setReceiveStatus/{id}")
    public String changeReceiveStatus(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        long giftId = Long.parseLong(id);
        giftRepository.changeReceiveStatus(giftId);
        giftRepository.changeDateOrder(giftId);
        redirectAttributes.addFlashAttribute("message", "Zrealizowano zamówienie");
        return "redirect:/gifts";
    }

    @Transactional
    @GetMapping("/deleteOrder/{id}")
    public String deleteOrder(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        long giftId = Long.parseLong(id);

        if ((giftRepository.checkOrderStatus(giftId) == 1) && (giftRepository.checkReceiveStatus(giftId) == 0)) {
            redirectAttributes.addFlashAttribute("message", "Nie możesz usunąć niedostarczonego zamówienia");
            return "redirect:/gifts";
        } else {

            if (giftRepository.checkReceiveStatus(giftId) == 0) {
                giftRepository.deleteById(giftId);
                redirectAttributes.addFlashAttribute("message", "Usunięto zamówienie z listy");
            } else {
                giftRepository.deleteById(giftId);
                redirectAttributes.addFlashAttribute("message", "Miłego korzystania z produktu :)");
            }
            return "redirect:/gifts";
        }


    }

}
