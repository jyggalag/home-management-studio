package com.jyggalag.controllers;

import com.jyggalag.models.Announcement;
import com.jyggalag.models.Gift;
import com.jyggalag.models.Task;
import com.jyggalag.repositories.AnnouncementRepository;
import com.jyggalag.repositories.GiftRepository;
import com.jyggalag.repositories.TaskRepository;
import com.jyggalag.services.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.List;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping("/")
public class HomeController {

    private TaskRepository taskRepository;
    private AnnouncementService announcementService;
    private AnnouncementRepository announcementRepository;
    private GiftRepository giftRepository;

    @Autowired
    public HomeController(
            TaskRepository taskRepository,
            AnnouncementRepository announcementRepository,
            GiftRepository giftRepository,
            AnnouncementService announcementService
    ) {
        this.announcementService = announcementService;
        this.taskRepository = taskRepository;
        this.giftRepository = giftRepository;
        this.announcementRepository = announcementRepository;
    }

    @GetMapping
    public String getHomePage(Model model, Principal principal) {
        List<Announcement> allAnnouncements = announcementRepository.findAllAnnouncements();
        int announcementsCount = allAnnouncements.size();

        List<Gift> allGifts = giftRepository.findAll();
        int giftsCount = allGifts.size();

        List<Task> allOpenTasks = taskRepository.getOpenTasks();
        int openTasksCount = allOpenTasks.size();

        List<Task> openTasksByUser = taskRepository.getTasksOwnedByUser(principal.getName());
        int openTasksByUserCount = openTasksByUser.size();

        List<Task> outstandingTasks = taskRepository.getOutstandingTasksByUser(principal.getName());
        int outstandingTasksCount = outstandingTasks.size();

        model.addAttribute("allOpenTasks", openTasksCount);
        model.addAttribute("openTasksByUser", openTasksByUserCount);
        model.addAttribute("outstandingTasks", outstandingTasksCount);
        model.addAttribute("allGifts", giftsCount);
        model.addAttribute("allAnnouncements", announcementsCount);
        model.addAttribute("loggedUser", principal.getName());
        return "home";
    }
}