package com.jyggalag.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
//@RequestMapping("/")
public class LoginController {

    @GetMapping("/login")
    public String login() {
        return "login_form";
    }
}
