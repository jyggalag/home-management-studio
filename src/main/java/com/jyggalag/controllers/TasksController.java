package com.jyggalag.controllers;

import com.jyggalag.config.MyDateFormatter;
import com.jyggalag.models.Task;
import com.jyggalag.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TasksController {
    private TaskRepository taskRepository;

    @Autowired
    public TasksController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Transactional
    @GetMapping
    public String showTasksList(Model model, Principal principal) {
        List<Task> tasks = taskRepository.findAll();
        tasks.sort(Comparator.comparing(Task::getStatus).thenComparing(Task::getDateEnd));

        for (Task task : tasks) {
            if (task.getDateEnd().compareTo(LocalDateTime.now().format(MyDateFormatter.noHoursFormatter())) < 1
            && task.getStatus() != 2) {
                taskRepository.changeStatusToOutstanding(task.getId());
            }
            if (task.getDateEnd().equals(LocalDateTime.now().format(MyDateFormatter.noHoursFormatter())) && task.getStatus() != 2) {
                taskRepository.changeStatusToWarning(task.getId());
            }
        }

        model.addAttribute("taskList", tasks);
        model.addAttribute("loggedUser", principal.getName());
        return "tasks/list";
    }

    @GetMapping("/add")
    public String moveToAdd(Model model, Principal principal) {
        model.addAttribute("taskModel", new Task());
        model.addAttribute("loggedUser", principal.getName());
        return "tasks/add";
    }

    @PostMapping("/add")
    public String addTask(@ModelAttribute Task taskModel, RedirectAttributes redirectAttributes, Principal principal) {
        taskModel.setOwner(principal.getName());
        taskModel.setDateAdded(LocalDateTime.now().format(MyDateFormatter.noHoursFormatter()));
        taskModel.setTakenBy("none");
        taskModel.setStatus(0);
        taskModel.setOutstandingFlag(0);

        switch (taskModel.getPriority()) {
            case 1:
                taskModel.setDateEnd(LocalDateTime.now().plusDays(3).format(MyDateFormatter.noHoursFormatter()));
                break;
            case 2:
                taskModel.setDateEnd(LocalDateTime.now().plusDays(7).format(MyDateFormatter.noHoursFormatter()));
                break;
            case 3:
                taskModel.setDateEnd(LocalDateTime.now().plusDays(14).format(MyDateFormatter.noHoursFormatter()));
                break;
            case 4:
                taskModel.setDateEnd(LocalDateTime.now().plusDays(30).format(MyDateFormatter.noHoursFormatter()));
                break;
            case 5:
                if (taskModel.getDateEnd().compareTo(LocalDateTime.now().toString()) < 0) {
                    redirectAttributes.addFlashAttribute("message", "Data zakończenia nie może byc mniejsza od daty złożenia.");
                    return "redirect:/tasks/add";
                } else {
                    taskModel.setDateEnd(taskModel.getDateEnd());
                }
                System.out.println(taskModel.getDateEnd());
                break;
            default:
                taskModel.setDateEnd(LocalDateTime.now().plusDays(60).format(MyDateFormatter.noHoursFormatter()));
                break;
        }

        taskRepository.save(taskModel);
        redirectAttributes.addFlashAttribute("message", "Pomyślnie dodano zadanie");
        return "redirect:/tasks";
    }

    @GetMapping("/details/{id}")
    public String showTask(@PathVariable("id") String id, Model model, Principal principal) {
        long taskId = Long.parseLong(id);
        Task workingTask = taskRepository.getSingleTask(taskId);
        model.addAttribute("task", workingTask);
        model.addAttribute("loggedUser", principal.getName());
        return "tasks/detail";
    }

    @Transactional
    @GetMapping("/details/takeTask/{id}")
    public String takeTaskByLoggedUser(@PathVariable("id") String id, Principal principal, RedirectAttributes redirectAttributes) {
        long taskId = Long.parseLong(id);
        taskRepository.changeTakenBy(principal.getName(), taskId);
        taskRepository.changeStatusToTaken(taskId);
        redirectAttributes.addFlashAttribute("message", "Przypisano zadanie.");
        return "redirect:/tasks/details/"+taskId;
    }

    @Transactional
    @GetMapping("/details/finishTask/{id}")
    public String finishTask(@PathVariable("id") String id, RedirectAttributes redirectAttributes, Principal principal) {
        long taskId = Long.parseLong(id);
        Task workingTask = taskRepository.getSingleTask(taskId);
        if (workingTask.getTakenBy().equals(principal.getName())) {
            workingTask.setCloseDate(LocalDateTime.now().format(MyDateFormatter.withHoursFormatter()));
            taskRepository.chngeStatusToFinish(taskId);
            redirectAttributes.addFlashAttribute("message", "Rozwiązano zadanie.");
        } else {
            redirectAttributes.addFlashAttribute("message", "Tylko osoba, która przejęła zadanie, może je zamknąć.");
        }
        return "redirect:/tasks/details/"+taskId;
    }

    @Transactional
    @GetMapping("/details/deleteTask/{id}")
    public String deleteTask(@PathVariable("id") String id, RedirectAttributes redirectAttributes, Principal principal) {
        long taskId = Long.parseLong(id);
        taskRepository.deleteSingleRow(taskId);
        return "redirect:/tasks";
    }


}
