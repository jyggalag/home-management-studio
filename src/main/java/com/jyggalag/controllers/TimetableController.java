package com.jyggalag.controllers;

import com.jyggalag.models.Timetable;
import com.jyggalag.models.User;
import com.jyggalag.repositories.TimetableRepository;
import com.jyggalag.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/timetable")
public class TimetableController {
    private TimetableRepository timetableRepository;

    @Autowired
    public TimetableController(TimetableRepository timetableRepository) {
        this.timetableRepository = timetableRepository;
    }

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public String showUsersList(Model model, Principal principal) {
        List<User> users = userRepository.findAll();
        model.addAttribute("loggedUser", principal.getName());
        model.addAttribute("userList", users);
        return "timetable/users";
    }

    @GetMapping("/{user}")
    public String showUserTimetable(@PathVariable("user") String user, Model model, Principal principal) {
        List<Timetable> userTimetable = timetableRepository.findByOwner(user);
        userTimetable.sort(Comparator.comparing(Timetable::getDateFrom));
        model.addAttribute("userTimetableList", userTimetable);
        model.addAttribute("loggedUser", principal.getName());
        model.addAttribute("user", user);
        return "timetable/list";
    }

    @GetMapping("/add")
    public String showAddRecord(Model model, Principal principal) {
        model.addAttribute("timetableModel", new Timetable());
        model.addAttribute("loggedUser", principal.getName());
        return "timetable/add";
    }

    @PostMapping("/add")
    public String addTimetableRecord(@ModelAttribute Timetable timetableModel, RedirectAttributes redirectAttributes, Principal principal) {
        timetableModel.setStatus(0);
        timetableModel.setOwner(principal.getName());
        timetableRepository.save(timetableModel);
        redirectAttributes.addFlashAttribute("Dodano termin");
        return "redirect:/timetable/"+timetableModel.getOwner();
    }

    @Transactional
    @GetMapping("/changeStatus/{id}")
    public String changeStatusOfRecord(@PathVariable("id") String id, RedirectAttributes redirectAttributes, Principal principal) {
        long timeTableId = Long.parseLong(id);
        timetableRepository.changeStatusToOne(timeTableId);
        redirectAttributes.addFlashAttribute("message", "Zakończono tydzień zjazdu");
        return "redirect:/timetable/" + principal.getName();
    }

    @Transactional
    @GetMapping("/deleteAll")
    public String flushTimetable(Principal principal, RedirectAttributes redirectAttributes) {
        String user = principal.getName();
        timetableRepository.flushTimetableByUser(user);
        redirectAttributes.addFlashAttribute("message", "Wyczyszczono tablicę zjazdów");
        return "redirect:/timetable/" + user;
    }

    @Transactional
    @GetMapping("/deleteEntry/{id}")
    public String deleteEntry(@PathVariable("id") String id, RedirectAttributes redirectAttributes, Principal principal) {
        long entryId = Long.parseLong(id);
        timetableRepository.deleteSingleEntry(entryId);
        redirectAttributes.addFlashAttribute("message", "Usunięto jeden rekord");
        return "redirect:/timetable/"+principal.getName();
    }

}
