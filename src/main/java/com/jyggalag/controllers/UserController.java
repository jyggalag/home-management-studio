package com.jyggalag.controllers;

import com.jyggalag.models.User;
import com.jyggalag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Controller
public class UserController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registermepls")
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "registerForm";
    }

    @Transactional
    @PostMapping("/registermepls")
    public String addUser(@ModelAttribute @Valid User user, BindingResult bindResult) {
        if (bindResult.hasErrors())
            return "registerForm";
        else {
            userService.addWithDefaultRole(user);
            return "redirect:/login";
        }
    }
}
