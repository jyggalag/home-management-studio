package com.jyggalag.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String owner;
    @NotEmpty
    private String title;
    @NotEmpty
    private String description;
    private String dateAdded;
    private String dateEnd;
    private int status;     //0 - oczekuje, 1 - w trakcie, 2 - zakonczone
    private String takenBy;
    private int priority;
    private String closeDate;
    private int outstandingFlag;    // 0 - OK, 1 - przedawnione, 2 - bliskie smierci

    public Task() {
    }

    public Task(
            Long id,
            String owner,
            @NotEmpty String title,
            @NotEmpty String description,
            String dateAdded, String dateEnd,
            int status, String takenBy,
            int priority,
            String closeDate,
            int outstandingFlag
    ) {
        this.id = id;
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.dateAdded = dateAdded;
        this.dateEnd = dateEnd;
        this.status = status;
        this.takenBy = takenBy;
        this.priority = priority;
        this.closeDate = closeDate;
        this.outstandingFlag = outstandingFlag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTakenBy() {
        return takenBy;
    }

    public void setTakenBy(String takenBy) {
        this.takenBy = takenBy;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public int getOutstandingFlag() {
        return outstandingFlag;
    }

    public void setOutstandingFlag(int outstandingFlag) {
        this.outstandingFlag = outstandingFlag;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dateAdded=" + dateAdded +
                ", dateEnd=" + dateEnd +
                ", isPending=" + status +
                ", takenBy='" + takenBy + '\'' +
                ", priority='" + priority + '\'' +
                ", closeDate='" + closeDate +
                ", outstandingFlag='" + outstandingFlag +
                '}';
    }
}
