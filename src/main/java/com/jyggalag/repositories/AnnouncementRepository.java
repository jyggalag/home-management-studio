package com.jyggalag.repositories;

import com.jyggalag.models.Announcement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnouncementRepository extends CrudRepository<Announcement, Long> {
    @Query("SELECT a FROM Announcement a ORDER BY a.date DESC")
    Page<Announcement> findAll(Pageable pageable);

    @Query("SELECT a FROM Announcement a")
    List<Announcement> findAllAnnouncements();

    @Modifying
    @Query("DELETE FROM Announcement a WHERE a.id=:id")
    void deleteEntityById(@Param("id") long id);
}
