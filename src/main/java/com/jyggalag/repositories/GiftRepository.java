package com.jyggalag.repositories;

import com.jyggalag.models.Gift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GiftRepository extends JpaRepository<Gift, Long> {

    @Modifying
    @Query("UPDATE Gift g SET g.orderStatus=1 WHERE g.id=:id")
    void changeOrderStatus(@Param("id") long id);

    @Modifying
    @Query("UPDATE Gift g SET g.receiveStatus=1 WHERE g.id=:id")
    void changeReceiveStatus(@Param("id") long id);

    @Modifying
    @Query("UPDATE Gift g SET g.dateOrder=DATE_FORMAT(NOW(), '%Y-%m-%d, %H:%i') WHERE g.id=:id")
    void changeDateOrder(@Param("id") long id);

    @Query("SELECT g.orderStatus FROM Gift g WHERE g.id=:id")
    int checkOrderStatus(@Param("id") long id);

    @Query("SELECT g.receiveStatus FROM Gift g WHERE g.id=:id")
    int checkReceiveStatus(@Param("id") long id);
}
