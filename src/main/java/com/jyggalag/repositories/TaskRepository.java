package com.jyggalag.repositories;

import com.jyggalag.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Modifying
    @Query("UPDATE Task t SET t.takenBy=:taker WHERE id=:id")
    void changeTakenBy(@Param("taker") String taker, @Param("id") long id);

    @Modifying
    @Query("UPDATE Task t SET t.status=1 WHERE id=:id")
    void changeStatusToTaken(@Param("id") long id);

    @Modifying
    @Query("UPDATE Task t SET t.status=2 WHERE id=:id")
    void chngeStatusToFinish(@Param("id") long id);

    @Modifying
    @Query("UPDATE Task t SET t.outstandingFlag=1 WHERE id=:id")
    void changeStatusToOutstanding(@Param("id") long id);

    @Modifying
    @Query("UPDATE Task t SET t.outstandingFlag=2 WHERE id=:id")
    void changeStatusToWarning(@Param("id") long id);

    @Modifying
    @Query("DELETE FROM Task t WHERE t.id=:id")
    void deleteSingleRow(@Param("id") long id);

    @Query("SELECT t FROM Task t WHERE t.id=:id")
    Task getSingleTask(@Param("id") long id);

    @Query("SELECT t FROM Task t WHERE t.takenBy=:user AND t.status=1")
    List<Task> getTasksOwnedByUser(@Param("user") String user);

    @Query("SELECT t FROM Task t WHERE t.status=0")
    List<Task> getOpenTasks();

    @Query("SELECT t FROM Task t WHERE t.outstandingFlag=1 AND t.takenBy=:user AND t.status=1")
    List<Task> getOutstandingTasksByUser(@Param("user") String user);
}
