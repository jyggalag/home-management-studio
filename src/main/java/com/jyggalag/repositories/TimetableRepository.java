package com.jyggalag.repositories;

import com.jyggalag.models.Timetable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimetableRepository extends JpaRepository<Timetable, Long> {

    @Query("SELECT t.dateFrom, t.dateTo FROM Timetable t WHERE t.owner=:user")
    List<String> selectUserDateFrom(@Param("user") String user);

    @Modifying
    @Query("UPDATE Timetable t SET t.status=1 WHERE t.id=:id")
    void changeStatusToOne(@Param("id") long id);

    @Modifying
    @Query("DELETE FROM Timetable t WHERE t.owner=:user")
    void flushTimetableByUser(@Param("user") String user);

    @Modifying
    @Query("DELETE FROM Timetable t WHERE t.id=:id")
    void deleteSingleEntry(@Param("id") long id);

    List<Timetable> findByOwner(String user);

}
