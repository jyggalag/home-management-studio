//package com.jyggalag.restcontrollers;
//
//import com.jyggalag.config.MyDateFormatter;
//import com.jyggalag.models.Announcement;
//import com.jyggalag.repositories.AnnouncementRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.time.LocalDateTime;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/rest/api")
//public class AnnouncementsControllerRest {
//
//    @Autowired
//    private AnnouncementRepository announcementRepository;
//
//    @RequestMapping(method = RequestMethod.GET, path = "/announcements", produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<Announcement> home() {
//        return announcementRepository.findAll();
//    }
//
//    @RequestMapping(method = RequestMethod.POST, path = "/announcements/add",
//        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public Announcement add(@RequestBody Map<String, String> body) {
//        Announcement announcement = new Announcement();
//        announcement.setTitle(body.get("title"));
//        announcement.setContent(body.get("content"));
//        announcement.setDate(LocalDateTime.now().format(MyDateFormatter.withHoursFormatter()));
//        return announcementRepository.save(announcement);
//    }
//}
