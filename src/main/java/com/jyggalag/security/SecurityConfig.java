package com.jyggalag.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/registermepls").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
//                    .loginPage("/login")
//                        .permitAll()
//                    .loginProcessingUrl("/processlogin")
//                        .permitAll()

                .and()
                    .logout()
                        .logoutUrl("/logmeout")
                        .logoutSuccessUrl("/login")
                        .permitAll();
    }
}
