package com.jyggalag.services;

import com.jyggalag.models.Announcement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

public interface AnnouncementService {
    Iterable<Announcement> listAllAnnouncements();
    Announcement saveAnnouncement(Announcement announcement);
    Page<Announcement> findAll(Pageable pageable);
    void deleteAnnouncementById(long id);
}
