package com.jyggalag.services;

import com.jyggalag.models.Gift;

import java.util.Optional;

public interface GiftService {
    Optional<Gift> findById(long id);

}
