package com.jyggalag.services;

import com.jyggalag.models.Gift;
import com.jyggalag.repositories.GiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GiftServiceImpl implements GiftService {

    @Autowired
    private GiftRepository giftRepository;

    @Override
    public Optional<Gift> findById(long id) {
        return giftRepository.findById(id);
    }


}
